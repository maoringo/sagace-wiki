#!/usr/local/bin/perl

package HESearcher;

use lib "/usr/local/sagace/lib/common/lib/";
use lib "lib";
use Data::Dumper;
use Search::Estraier_CST;
use base qw(Class::Accessor);
HESearcher->follow_best_practice;
HESearcher->mk_accessors(qw(action facet1 facet2 meta synonym_list db expand nres duplicate_count countSearch db));

use lib "/usr/local/sagace/lib/web";
use lib "/usr/local/sagace/lib/common";
use Encode;
use Encode qw(decode);
use DatabaseReader;
use SagaceConfig;
use Common;
use Taxonomy;
use EncryptCaller;
use ICD10;

#use utf8;
#use encoding utf8;

my $common = Common->new;

my $action;
my $keyword;
my $synonym_list;
my $expand;
my $facet1;
my $facet2;
my $facet3;
my $skip;
my $meta;
my $lang;
my $encoded_keyword;
my $encoded_facet1;
my $encoded_facet2;
my $encoded_facet3;
my $alldbs;
my $speciesImageList;
my $random;
my $dbs;
my $dbnames;
my $db_nodeurl;
my $icd10List;
my $countSearch;
my $db;
my $patentNode;

sub initialize{
	my $self = shift;
	$action = $self->{action};
	$keyword = $self->{keyword};
	$synonym_list = $self->{synonym_list};
	$expand =$self->{expand} || "";
	$facet1 = $self->{facet1} || "";
	$facet2 = $self->{facet2} || "";
	$facet3 = $self->{facet3} || "";
	$lang = $self->{lang} || "";
	$skip = $self->{skip};
	$meta = $self->{meta};
	$db = $self->{db};
	$random = $self->{random};
	$dbs = $self->{dbs} || "";
	$countSearch = $self->{countSearch} || "";
	$encoded_keyword = $common->xss_encode($keyword);
	my $config = SagaceConfig->new;
	$config->initialize();
	my $facet_file = $config->get("facet");
	my $db_file = $config->get("database");
	my $dbReader = DatabaseReader->new({"db_file"=>$db_file,"facet_file"=>$facet_file});
	$alldbs = $dbReader->getAllDatabases();
	$dbnames = $dbReader->get("dbnames");
	$db_nodeurl = $dbReader->get("db_nodeurl");
	$patentNode = $config->get("node_master_url") . "/patent";
}

sub search{
	my $self = shift;
	#node_url、top_nodeのパラメータの取得。
	my $config = SagaceConfig->new;
	$config->initialize();
	my $node_master_url = $config->get("node_master_url");
	my $top_node = $config->get("top_node");
	
	my @db_hit_counts = ();
	
	# create and configure node
	#facet1,facet2 画面左のファセットタグをクリックしたとき
		
	# create condition
	my $cond = new Search::Estraier::Condition;
	#簡便書式
	#$cond->set_options('SIMPLE');
	#$cond->set_options('USUAL');
	$cond->set_options(qw/USUAL SIMPLE/ );
	
	#同じDBからは１つの結果しか返さない
	#$cond->set_distinct('@db');
	
	#メタ情報での絞り込み
	#if($action eq "species" || $action eq "db" || $action eq "taxonID"){
	if($action eq "species"  ){
		$cond->add_attr("\@BiologicalDatabaseEntry_taxon_BiologicalDatabaseEntry_name STREQ $meta" );
	}
	elsif($action eq "taxonID"){
		$cond->add_attr("\@BiologicalDatabaseEntry_taxon_BiologicalDatabaseEntry_entryID STREQ $meta");
	}
	elsif($action eq "availability"){
		$cond->add_attr("\@BiologicalDatabaseEntry_ResourceAvailability STREQ $meta");
	}
	elsif($action eq "lastM"){
		$cond->add_attr("\@BiologicalDatabaseEntry_dateModified STREQ $meta");
	}
	
	#データベースでの絞り込み
	my $db_id = $dbnames->{$dbs};
	#print $dbs , " " , $db_id;
	if($dbs ne "" && defined($db_id)){
		$cond->add_attr("\@db STREQ $db_id");
	}

		 
	# set search phrase
	#my $synonym_list = search_integ_dictionary($keyword);
	#同義語展開があるときとないとき
	if(!defined($expand) || $expand eq ""){
		my $search_phrase = createSearchPhrase();
		$cond->set_phrase($search_phrase);			
	}
	else{
		my $search_phrase = createSearchPhrase_OffExpansion();
		#print $search_phrase;
		$cond->set_phrase($search_phrase);
	}
		
		
	my $node;	
	
	#ファセットが２つ選択されてAND検索を行う場合。除外リンクとして下記メソッドを呼ぶ
	if($action ne "db" && $facet1 ne "" && $facet2 ne "" && $facet3 eq ""){
		my $node_except = facetsANDNode($node_master_url,$facet1,$facet2);
		$node = $node_except->[0];
		my $maskNodes = $node_except->[1];
		my $nodelinks = $node_except->[2];
		#print join(" " , @$maskNodes);
		my $length = @$maskNodes;
		#print "<br>$length";
		if($length!=0){
			$cond->set_nodelinks(@$nodelinks);
			$cond->set_mask(@$maskNodes);
			#$cond->set_mask(qw/8/)
		}
	}
	elsif($action ne "db" && $facet1 ne "" && $facet2 eq "" && $facet3 ne ""){
		my $node_except = facetsANDNode($node_master_url,$facet1,$facet3);
		$node = $node_except->[0];
		my $maskNodes = $node_except->[1];
		my $nodelinks = $node_except->[2];
		#print join(" " , @$maskNodes);
		my $length = @$maskNodes;
		#print "<br>$length";
		if($length!=0){
			$cond->set_nodelinks(@$nodelinks);
			$cond->set_mask(@$maskNodes);
			#$cond->set_mask(qw/8/)
		}
	}
	elsif($action ne "db" && $facet1 eq "" && $facet2 ne "" && $facet3 ne ""){
		my $node_except = facetsANDNode($node_master_url,$facet2,$facet3);
		$node = $node_except->[0];
		my $maskNodes = $node_except->[1];
		my $nodelinks = $node_except->[2];
		#print join(" " , @$maskNodes);
		my $length = @$maskNodes;
		#print "<br>$length";
		if($length!=0){
			$cond->set_nodelinks(@$nodelinks);
			$cond->set_mask(@$maskNodes);
			#$cond->set_mask(qw/8/)
		}
	}
	elsif($action ne "db" && $facet1 ne "" && $facet2 ne "" && $facet3 ne ""){
		my $node_except = facetsANDNodeTripple($node_master_url);
		$node = $node_except->[0];
		my $maskNodes = $node_except->[1];
		my $nodelinks = $node_except->[2];
		#print join(" " , @$maskNodes);
		my $length = @$maskNodes;
		#print "<br>$length";
		if($length!=0){
			$cond->set_nodelinks(@$nodelinks);
			$cond->set_mask(@$maskNodes);
			#$cond->set_mask(qw/8/)
		}
	}
	else{
		#データベースリストから選択した場合　または　
		if($action eq "db"){	
			if($meta !~ m/jp_patent/){
				my $dbList = $alldbs->{$meta};
				$node = new Search::Estraier::Node($dbList->[11]);
			}
			else{
				$node = new Search::Estraier::Node($patentNode);
				$cond->add_attr("\@db STRBW $meta");
			}
		}
		#データベースリストから選択して検索後、リッチスニペットを指定した場合
		elsif($db ne ""){
			my $dbList = $alldbs->{$db};
			$node = new Search::Estraier::Node($dbList->[11]);
		}
		elsif($facet1 ne ""){
			if(	$countSearch ne ""){
				$node_master_url = $config->get("node_count_url");
			}
			$node = new Search::Estraier::Node("$node_master_url/$facet1");
		}
		elsif($facet2 ne ""){
			$node = new Search::Estraier::Node("$node_master_url/$facet2");
		}
		elsif($facet3 ne ""){
			$node = new Search::Estraier::Node("$node_master_url/$facet3");
		}
		elsif($action eq "facet1" || $action eq "facet2" || $action eq "facet3"){
			$node = new Search::Estraier::Node("$node_master_url/$action");
		}
		elsif($action eq "search"){
			$node = new Search::Estraier::Node("$node_master_url/$top_node");
		}
		else{
			$node = new Search::Estraier::Node("$node_master_url/$top_node");
		}
		
	
	}
		$node->set_snippet_width(300, 96, 	96); 
		#$node->set_timeout(5);#タイムアウトの時間設定 5秒	
		$cond->set_max(10);
		$cond->set_skip($skip);
		#検索を実行 
		#print $node->cond_to_query($cond,1);
		my $nres = $node->search($cond, 5);
		#print Dumper($nres);
#		my %hints = $nres->hints();
		
		#print Dumper(\%hints);
#		#print %hints;
#		foreach my $test(keys( %hints )){
#			#print $test;
#			#print Dumper($test);
#			foreach my $w(values(%$test)){
#				print $w;
#			}
##			print "test" , $hints->{$test};
##			
##			print $test->{'LINK#1'};
#		     foreach my $elem (keys %{$hints->{$test}}) {
#		            print "  $elem: " . $hints->{$test}->{$elem} . "\n";
#		        }
#		}
		#print $nres->hint('LINK#2');
		#print $nres->hint('NODE');
		
		$self->{nres} = $nres;
}

#ファセットノードが３つとも指定されたときの絞込みメソッド。
#facet1とfacet2の間でAND検索を行う際に、両者で重ならないノードを取得する。→検索結果から除外する。
#HyperEstraierの仕様なのかわからないが、maskする数が多いとうまくmaskがかからないための処置。
#ただし、estmaster.cのMASKLINKMAXを修正し、メソッドも修正する必要あり。
#ノードにリンクされている数が少ない方に対して、maskをかける
sub facetsANDNodeTripple{
	my ($node_master_url) = @_;
	my $facet_node1 = new Search::Estraier::Node("$node_master_url/$facet1");
	my $facet_node2 = new Search::Estraier::Node("$node_master_url/$facet2");
	my $facet_node3 = new Search::Estraier::Node("$node_master_url/$facet3");
	my $node1 = $facet_node1->links;#@{ $node->links }
	my $node2 = $facet_node2->links;
	my $node3 = $facet_node3->links;
	my $length1 = @$node1;
	my $length2 = @$node2;
	my $length3 = @$node3;
	my @mask_nodes = ();
	my $node_org;
	my $node_comp1;
	my $node_comp2;
	my $length;
	my $facet_node;
	my @nodelinks=();
	#ノードにリンクされている数が少ない方に対して、maskをかける
	if($length2<=$length1 && $length1<=$length3){
		$node_org = $node2;
		$node_comp1 = $node1;
		$node_comp2 = $node3;
		$length = $length2;
		$facet_node = $facet_node2;
	}
	elsif($length1<=$length2 && $length2<=$length3){
		$node_org = $node1;
		$node_comp1 = $node2;
		$node_comp2 = $node3;
		$length = $length1;
		$facet_node = $facet_node1;
	}
	else{
		$node_org = $node3;
		$node_comp1 = $node2;
		$node_comp2 = $node1;
		$length = $length3;
		$facet_node = $facet_node3;
	}
	for(my $i=0;$i<$length;$i++){
		my $flag = "false";
		my @link2s = split(/\t/,$node_org->[$i]);
		my $link2 = $link2s[0];
		push(@nodelinks,$i);
		#print $link1 , "<br>";
		foreach my $link1_(@$node_comp1){
			my @link1s = split(/\t/,$link1_);
			my $link1 = $link1s[0];
			#print "$link2<br>";
			if($link1 eq $link2){
				foreach my $link3_(@$node_comp2){
					my @link3s = split(/\t/,$link3_);
					my $link3 = $link3s[0];
					if($link3 eq $link2){
						$flag = "true";
						#print "$link1 : $link2<br>";
						last;
					}
				}
			}
		}
		if($flag eq "false"){
			push(@mask_nodes,$i+1);
		}
	} 
	my @result = ();
	push(@result,$facet_node);
	push(@result,\@mask_nodes);
	push(@result,\@nodelinks);
	return \@result;
}


#2つのノードで絞込みをかけるとき
sub facetsANDNode{
	my ($node_master_url,$selectedFacet1,$selectedFacet2) = @_;
	my $facet_node1 = new Search::Estraier::Node("$node_master_url/$selectedFacet1");
	my $facet_node2 = new Search::Estraier::Node("$node_master_url/$selectedFacet2");
	#print $selectedFacet1 , "<br> $selectedFacet2<br>";
	my $node1 = $facet_node1->links;#@{ $node->links }
	my $node2 = $facet_node2->links;
	my $length1 = @$node1;
	my $length2 = @$node2;
	#print $length1 , " " , $length2 , "<br>";
	my @mask_nodes = ();
	my $node_org;
	my $node_comp;
	my $length;
	my $facet_node;
	my @nodelinks=();
	#ノードにリンクされている数が少ない方に対して、maskをかける
	if($length2<=$length1){
		$node_org = $node2;
		$node_comp = $node1;	
		$length = $length2;
		$facet_node = $facet_node2;
	}
	else{
		$node_org = $node1;
		$node_comp = $node2;
		$length = $length1;
		$facet_node = $facet_node1;
	}
	
	for(my $i=0;$i<$length;$i++){
		my $flag = "false";
		my @link2s = split(/\t/,$node_org->[$i]);
		my $link2 = $link2s[0];
		push(@nodelinks,$i);
		#print $link1 , "<br>";
		foreach my $link1_(@$node_comp){
			my @link1s = split(/\t/,$link1_);
			my $link1 = $link1s[0];
			#print "$link2<br>";
			if($link1 eq $link2){
				$flag = "true";
				#print "$link1 : $link2<br>";
				last;
			}
		}
		if($flag eq "false"){
			push(@mask_nodes,$i+1);
		}
	} 
	my @result = ();
	push(@result,$facet_node);
	push(@result,\@mask_nodes);
	push(@result,\@nodelinks);
	return \@result;
}





#検索に必要なフレーズを作成する。
sub createSearchPhrase{
	my $search_phrase = "";
	my @key = keys(%$synonym_list);
	#foreach my $word(keys(%$synonym_list)){
	for(my $i=0;$i<$#key+1;$i++){
		my $word = $key[$i];
		#print $word , "<br>";
		if(!($word =~ m/^AND_/ || $word =~ m/^OR_/ || $word =~ m/^ANDNOT_/ || $word =~ m/^NOSYNONYM/)){
			my $synonym = $synonym_list->{$word};
			my $length = @$synonym;
			for(my $i=0;$i<$length;$i++){
				$search_phrase .= "\"" . $synonym->[$i][3] . "\"";
				if($i!=$length-1){
					$search_phrase .= " | ";
				}
			}
		}
		elsif($word =~ m/^NOSYNONYM/){
			my $no_synonym = $synonym_list->{$word};
			$search_phrase .= "\"" . $no_synonym . "\"";
		}
		#else{
		if($i!=$#key){
			if($word =~ m/^AND_/){
				$search_phrase .= " & ";
			}
			elsif($word =~ m/^OR_/){
				$search_phrase .= " | ";
			}
			elsif($word =~ m/^ANDNOT_/){
				$search_phrase .= " ! ";
			}
			else{
				#$search_phrase .= " & ";
			}		
		}
		
		#}
	}
	
	return $search_phrase;
}

#同義語展開をOFFにしたときの検索フレーズを作成する。
sub createSearchPhrase_OffExpansion{
	my $search_phrase = "";
#	foreach my $word(keys(%$synonym_list)){
	my @key = keys(%$synonym_list);
	for(my $i=0;$i<$#key+1;$i++){
		my $word = $key[$i];
		if(!($word =~ m/^AND_/ || $word =~ m/^OR_/ || $word =~ m/^ANDNOT_/ || $word =~ m/^NOSYNONYM/)){
			$search_phrase .= "\"" . $word . "\"";
		}
		elsif($word =~ m/^NOSYNONYM/){
			my $no_synonym = $synonym_list->{$word};
			$search_phrase .= "\"" . $no_synonym . "\"";
		}
		#else{
		if($i!=$#key){
			if($word =~ m/^AND_/){
				$search_phrase .= " & ";
			}
			elsif($word =~ m/^OR_/){
				$search_phrase .= " | ";
			}
			elsif($word =~ m/^ANDNOT_/){
				$search_phrase .= " ! ";
			}
			else{
				#$search_phrase .= " & ";
			}		
		}
		#}
	} 
	return $search_phrase;
}


#id検索と普通の検索で、同じurlが返ってきたら同一とみなす。
sub checkDuplicateResult{
	my($id_nres,$rdoc) = @_;
	if(!defined($id_nres)){
		return "false";
	}
	
	for my $j ( 0 ... $id_nres->doc_num - 1 ) {
		my $id_rdoc = $id_nres->get_doc($j);
		if($rdoc->attr('@uri') eq $id_rdoc->attr('@uri')){
			return "true";
		}
	}
	return "false";
}

#検索結果を表示する。
sub createSearchResultHTML{
	my $self = shift;
	my ($id_nres) = @_; 
	my $nres = $self->{nres};
	my $text = "";
	my $duplicate_count=0;
	#結果を分解して表示する。
	if (defined($nres)) {
		my $taxon = Taxonomy->new;
		my $taxonomy = $taxon->getTaxonomy($nres);
		
		 # for each document in results
		 for my $i ( 0 ... $nres->doc_num - 1 ) {
		      # get result document
		      my $rdoc = $nres->get_doc($i);
		     
		      #重複確認。
		      my $duplicate = checkDuplicateResult($id_nres,$rdoc);
		      if($duplicate eq "true"){
                  #print "test";
                  $duplicate_count++;
                  next;
              }
		     
		     # print $nres->hint("LINK#1");
		     #文字列の成形
		     my $seealso = editSeeAlso($rdoc);
		     my $image = editImage($rdoc);
		     my $title = editTitleSnippet($rdoc->attr('@title'),"TITLE",$rdoc);
		     my $url = editURL($rdoc->attr('@uri'));
		     my $snippet =  editTitleSnippet($rdoc->snippet,"SNIPPET",$rdoc);#$rdoc->attr('@display');
		     my $category =editCategory($rdoc);
		     my $species = editSpecies($rdoc,$taxonomy);
		     my $data_id = editID($rdoc);
		     my $molecule = editmolecule($rdoc);
		     my $author = editauthor($rdoc);
		     my $lastModified = editLastModified($rdoc);
		     my $dateCreated = editDateCreated($rdoc);
		     my $exptlMethod = editexptlMethod($rdoc);
		     my $resolution = editResolution($rdoc);
		     my $dateDeposition = editDateDeposition($rdoc);
		     my $availability = editResourceAvailability($rdoc);
		     my $originalTissue = editOriginalTissue($rdoc);
			 my $disease = editDisease();
			 my $provider = editProvider($rdoc);
			 my $reference = editReference($rdoc);
			 my $referencep = editReferencep($rdoc);
			 my $referencepid = editReferencepid($rdoc);
		     
		     my $database = $rdoc->attr('@db');
			 my $id = $rdoc->attr('#nodelabel');
			 my $db_property = $alldbs->{$id};
		     my $rank = $skip + $i + 1; #順位
		     #$text .= "<P style='width:600px;'>\n";
		    my $agent = $ENV{'HTTP_USER_AGENT'};
		    if($agent =~ m/Macintosh/ig){
			$text .= "<P style='width:600px'>\n";
	            } 
	            else{
 			$text .= "<P>\n";
		    }

		    
		    #タイトル表示
		     $text .= "<a href='" . Common->xss(encryptURL($rdoc->attr('@uri'))) . "' target='_blank'  class='sitelink' style='text-decoration:none;font-weight:normal;color:#1122CC' onmousedown=\"getLog('" . $encoded_keyword . "','". Common->xss_encode($rdoc->attr('@uri')) . "','" . Common->xss_encode($db_property->[7]) . "','" . $rank . "')\">" . $title . "</a>"  ."<br>"."\n";
		     
		                
		     #URL、データベース名、組織名の表示
		     $text .= "<span class='green'>" . $url . "</span>&nbsp;&nbsp;&nbsp;<span class='rcategory'>" . editNodeLabel($rdoc) . "</span><br>" ;
		     
		     #画像があれば表示
		     $text .= $image;
		                
	         #カテゴリ.Species. ID. 分譲可の表示
		     $text .= "<span class='rcategory'>$category</span>";
			if($data_id ne "" or $species ne "" or $disease ne "" or $lastModified ne "" or $originalTissue ne "" or $availability ne ""){ 
                        $text .="<br>";
                        }    
		     
		     #data id の表示
		     if($data_id ne ""){
		     	$text .= "<span class='rcategory2'>&nbsp;-&nbsp;" . $data_id . "</span>";
		     }
		      #molecule の表示 
		     if($molecule ne ""){
		     	$text .= "<span class='rcategory2'>&nbsp;-&nbsp;" . $molecule . "</span>";
		     }
		      #author の表示 
		     if($author ne ""){
		     	$text .= "<span class='rcategory2'>&nbsp;-&nbsp;" . $author . "</span></br>";
		     }
		     
		     #speciesの表示
		     if($species ne ""){
		     	$text .= "<span class='rcategory'>&nbsp;-&nbsp;" .  $species . "</span>";
		     }
		     
		     #providerの表示
		     if($provider ne ""){
		     	$text .= "<span class='rcategory2'>&nbsp;-&nbsp;" .  $provider . "</span>";
		     }
		     
		     #疾患の表示
		     if($disease ne ""){
		     	$text .= "<span class='rcategory2'>&nbsp;-&nbsp;" .  $disease . "</span>";
		     }
		     #登録日の表示
		     if($dateDeposition ne ""){
		     	$text .= "<span class='rcategory2'>&nbsp;-&nbsp;" .  $dateDeposition . "</span>";
		     }

		     #作成日の表示
		     if($dateCreated ne ""){
		     	$text .= "<span class='rcategory2'>&nbsp;-&nbsp;" .  $dateCreated . "</span>";
		     }

		     #最終更新日の表示
		     if($lastModified ne ""){
		     	$text .= "<span class='rcategory'>&nbsp;-&nbsp;" .  $lastModified . "</span>";
		     }

		     #実験手法の表示
		     if($exptlMethod ne ""){
		     	$text .= "<span class='rcategory2'>&nbsp;-&nbsp;" .  $exptlMethod ;
		    	 if($resolution ne ""){
		     	$text .= " (" .  $resolution . ") ";
		     	}
			$text .= "</span>";
		     }
		     
		     #由来組織の表示
		     if($originalTissue ne ""){
		     	$text .= "<span class='rcategory2'>&nbsp;-&nbsp;" .  $originalTissue . "</span>";
		     }
			 
		     #分譲可否の表示
		     if($availability ne ""){
		     	$text .= "<span class='rcategory'>&nbsp;-&nbsp;" .  $availability . "</span>";
		     }
		    
	             #PubMedの表示 
		     if($referencep ne ""){
		     	#$text .= "<span class='rcategory2'>&nbsp;-&nbsp;Reference：</span><span class='rcategory' itemprop='reference' content='pmid:".$referencep."'><a href=\"http://www.ncbi.nlm.nih.gov/pubmed/?term=" .  $referencep  .'">'. $referencep . "</a></span>";
		     	$text .= "</br><span class='rcategory2'><a href=\"http://www.ncbi.nlm.nih.gov/pubmed/?term=". $referencepid. '" style=\'color:#3C98C8\';>' .  $referencep . "</a></span>";
		     }
	
		     $text .= "<br>";
		     
		                
		     #スニペットの表示
		
		     if($database ne "pdbj"){ 
		     $text .= "<span class='rsummary'>" . $snippet . " </span>";
			}
		     
		     #see alsoの表示
		     $text .= $seealso;
		     
		     #referenceの表示
		     if($reference ne ""){
		     #現状，１つの場合のみ表示させる。by Maori
                                @referencearray = split(/:+|,/,$reference);
                       #中身がない場合には表示させない
                        if ($referencearray[1] == ""){}
                        elsif($reference =~ /PubMed/ or $reference =~ /pmid/){
                                $referenceurl = "http://www.ncbi.nlm.nih.gov/pubmed/?term=".$referencearray[1];
			$text .= "</br>文献:&nbsp;-&nbsp;<a href=\"" .  $referenceurl . "\" itemprop='reference' content='pmid:". $referencearray[1] ."'>PubMed:". $referencearray[1] ."</a>";;
                        }
			else{
                     $text .= "</br>文献:&nbsp;-&nbsp;<a href=\"" .  $reference . "\" itemprop='reference'>". $reference ."</a>";;
                        }
		      } 
		     
		     
		     $text .= "</P>";
		}
	} 
	else {
		        #die "error: ", $node->status,"\n";
	}
	$self->{duplicate_count} = $duplicate_count;
	return $text;
}

#特許とPNEに関しては暗号化する。それ以外はそのまま。
sub encryptURL{
	my($url) = @_;
	if($url =~ m/biosciencedbc.jp\/dbsearch\/Patent/i){
		my $encrypter = new EncryptCaller->new();
		my $patentURL = $encrypter->encryptPatent($url);
		return $patentURL;
	} 
	elsif($url =~ m/lifesciencedb.jp\/dbsearch\/Literature\/get_pne_cgpdf/i){
		my $encrypter = new EncryptCaller->new();
		my $pneURL = $encrypter->encryptPNE($url);
		return $pneURL;
	}
	else{
		return $url;
	}
}

sub editDisease{
	my $term = "";
	if(defined($icd10List)){
	   	if($lang eq ""){
	   		$term = "Disease: " . $icd10List->[0]->[3];
	   	}
	   	else{
	   		$term = "Disease: " . $icd10List->[0]->[2];
	   	}	     	
	}
	return $term;
}
#
#sub getTaxonomy{
#	my($nres) = @_;
#	my $taxonids = "";
# 	for my $i ( 0 ... $nres->doc_num - 1 ) {
#		 my $rdoc = $nres->get_doc($i);	
#		 my $taxonid = $rdoc->attr('@taxonID');
#
#		 if(defined($taxonid)){
#		 	$taxonids .= $taxonid .",";
#		 }
# 	}
# 	if($taxonids ne ""){
# 		my $length = length($taxonids);
# 		$taxonids = substr($taxonids,0,$length-1);
# 		my $taxonomy = Taxonomy->new;
# 		my $result = $taxonomy->getTaxonomy($taxonids);
# 		return $result;
# 	}
# 	
# 	
# 	return "";
#}

#該当キーワードを太字にする。
sub editTitleSnippet{
	my($snippet,$type,$rdoc) = @_;
        my $database = $rdoc->attr('@db');
        my $pdbjtitle = $rdoc->attr('@BiologicalDatabaseEntry_description');
	#タイトルの文字数が２５文字以上はカットする。
	if($type eq "TITLE"){
		utf8::decode($snippet); #utf8フラグをたてる
		my $length = length($snippet);#length(decode('utf-8', $snippet));
		my $count = 0;
		my $newSnippet = "";
		my $flag = 0;
		for(my $i=0;$i<$length;$i++){
			my $c = substr($snippet, $i, 1);
			if($c =~ m/[a-zA-Z]/g){
				$count++;;
			}
			else{
				$count += 2;
			}
			if($count>80){
				$flag = 1;
				last;
			}
			$newSnippet .= $c;
		}
		$snippet = $newSnippet;
		if($flag==1){
			$snippet .= ". . .";
		}
		if($database eq "pdbj"){
			$snippet = substr($pdbjtitle,0,100); 
			if(length($snippet) > 99){
			$snippet .= ". . .";
			}
		}

		utf8::encode($snippet); #utf8フラグをおろす→文字化け対策
		
	}
	$snippet = Common->xss($snippet);
	foreach my $word(keys(%$synonym_list)){
		if(!($word =~ m/^AND_/ || $word =~ m/^OR_/ || $word =~ m/^ANDNOT_/ || $word =~ m/^NOSYNONYM/)){
			my $synonym = $synonym_list->{$word};
			my $length = @$synonym;
			#同義語展開ありのとき
			if(!defined($expand) || $expand eq ""){
				for(my $i=0;$i<$length;$i++){
					my $esc_term = $common->escape($synonym->[$i][3]);
					$snippet =~ s/($esc_term)\t($esc_term)/$synonym->[$i][3]/ig;
					$snippet =~ s/($esc_term)/<strong>$1<\/strong>/ig;
					$snippet =~ s/\n|\r//ig;
				}
			}
			#同義語展開なしのとき
			else{
				$snippet =~ s/($word)\t($word)/$word/ig;
				$snippet =~ s/($word)/<strong>$1<\/strong>/ig;
				$snippet =~ s/\n|\r//ig;
			}

		}
		elsif($word =~ m/^NOSYNONYM/){
			my $no_synonym = $synonym_list->{$word};
			$snippet =~ s/($no_synonym)\t($no_synonym)/$no_synonym/ig;
			$snippet =~ s/($no_synonym)/<strong>$1<\/strong>/ig;
			$snippet =~ s/\n|\r//ig;
		}
	}

	if($type eq "SNIPPET"){
	$snippet .= " .....";}
	return $snippet;
}
#ドメイン名までを抽出する。
sub editURL{
	my($url) = @_;
	my @array = split(/\//,$url);
	my $new_url = $array[0] . "//" . $array[1] . $array[2] . "/"; 
	return $common->xss($new_url);
}

##medals,niasのノードIDが例外的な記述をされているため例外処理を加える。
#sub exception{
#	my($id) = @_;
#	my @medals = ("trap","ppiview","knapsack_family","knapsack","hangel","dnaplobelocator","confc","attedii","archgenet","archaic");
#	my @nias = ("zootechnical","niah","gaichu","apasd","agritogo");
#	foreach my $except(@medals){
#		if($except eq $id){
#			my $ex = "medals_" . $id;
#			return $ex;
#		}
#	}
#	foreach my $except(@nias){
#		if($except eq $id){
#			my $ex = "nias_" . $id;
#			return $ex;
#		}
#	}
#	return $id;
#}

sub exceptNodeURL{
	("Ritsumeikan Univ.");
}

sub editImage{
	my($rdoc) = @_;
	my $image_url = $rdoc->attr('@BiologicalDatabaseEntry_image');
	if(!defined($image_url)){
		return "";
	}
	
	my @images = split(/,|;/,$image_url);
	my $image = "<img src=\"$images[0]\" width=\"100\" height=\"70\" align=\"left\" style=\"margin: 0px 3px 0px 0px;\">\n";
	return $image;
}

sub editLastModified{
	my($rdoc) = @_;
	my $lastModified = $rdoc->attr('@BiologicalDatabaseEntry_dateModified');
	if(!defined($lastModified)){
		return "";
	}
	
	return "Last Modified: <a href='search.cgi?action=lastM&keyword=$encoded_keyword&facet1=$facet1&facet2=$facet2&facet3=$facet3&meta=$lastModified&expand=$expand&lang=$lang&random=$random&db=$db' onclick=\"showLoadingImg()\">" . $lastModified . "</a>";
}

sub editDateCreated{
	my($rdoc) = @_;
	my $database = $rdoc->attr('@db');
	my $dateCreated = $rdoc->attr('@BiologicalDatabaseEntry_dateCreated');
	if(!defined($dateCreated)){
		return "";
	}
#pdbj対策
	if($database eq "pdbj"){	
	return "Release: " . $dateCreated;
	}
	else{	
	return "Date Created: " . $dateCreated;
	}
}

sub editDateDeposition{
	my($rdoc) = @_;
	my $dateDeposition = $rdoc->attr('@PDBo_database_PDB_rev.date_original');
	if(!defined($dateDeposition)){
		return "";
	}
	return "Deposition: " . $dateDeposition;

}


sub editReference{
	my($rdoc) = @_;
	my $reference = $rdoc->attr('@BiologicalDatabaseEntry_reference');
	if(!defined($reference)){
		return "";
	}
	if($lang ne ""){
                return "Reference: " . $reference;
        }
        else{
                return  $reference;
	}	
}

sub editReferencepid{
	my($rdoc) = @_;
	my $referenceid = $rdoc->attr('@BiologicalDatabaseEntry_reference_PubMed');
	if(!defined($referenceid)){
		return "";
	}
                return  $referenceid;
}

sub editReferencep{
use XML::Simple;
use LWP;
my $reference = '';
	my($rdoc) = @_;
	my $referencep = $rdoc->attr('@BiologicalDatabaseEntry_reference_PubMed');
	if(!defined($referencep) or $referencep eq ""){
		return "";
	}
	else{
my $api = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=xml&id=';

my $ua = LWP::UserAgent->new;
my $res = $ua->get(
  $api.$referencep
);
my $document = $res->content
    or die "cannot get content from url";

my $parser = XML::Simple->new;
my $data = $parser->XMLin($document);

my $reference = ' - Reference: ';
$reference .= $data->{PubmedArticle}->{MedlineCitation}->{Article}->{ArticleTitle}. "\n";
$reference .= $data->{PubmedArticle}->{MedlineCitation}->{Article}->{Journal}->{ISOAbbreviation}. ", ";
$reference .= $data->{PubmedArticle}->{MedlineCitation}->{Article}->{Journal}->{JournalIssue}->{Volume}. ", ";
$reference .= $data->{PubmedArticle}->{MedlineCitation}->{Article}->{Journal}->{JournalIssue}->{PubDate}->{Year}. "\n";

	if($lang ne ""){
                return $reference;
        }
        else{
                return $reference;
	}	
}
}

sub editProvider{
	my($rdoc) = @_;
	my $provider = $rdoc->attr('@BiologicalDatabaseEntry_provider');
	if(!defined($provider)){
		return "";
	}
	
	return "Provider: " . $provider;
}

sub editexptlMethod{
	my($rdoc) = @_;
	my $method = $rdoc->attr('@PDBo_exptl.method');
	if(!defined($method)){
		return "";
	}
	
	return "Method: " . $method;
}

sub editResolution{
	my($rdoc) = @_;
	my $resolution = $rdoc->attr('@PDBo_refine.ls_d_res_high');
	if(!defined($resolution)){
		return "";
	}
	
	return $resolution;
}

sub editOriginalTissue{
	my($rdoc) = @_;
	my $lastModified = $rdoc->attr('@BiologicalDatabaseEntry_OriginalTissue');
	if(!defined($lastModified)){
		return "";
	}
	
	if($lang ne ""){
		return "Original Tissue: " . $lastModified;
	}
	else{
		return "採取組織: " . $lastModified;
	}
	
}

sub editResourceAvailability{
	my($rdoc) = @_;
	my $lastModified = $rdoc->attr('@BiologicalDatabaseEntry_ResourceAvailability');
	if(!defined($lastModified)){
		return "";
	}
	
	if($lang ne ""){
		if($lastModified eq "F"){
			return "<a href='search.cgi?action=availability&keyword=$encoded_keyword&facet1=$facet1&facet2=$facet2&facet3=$facet3&meta=F&expand=$expand&lang=$lang&random=$random&db=$db' onclick=\"showLoadingImg()\">Resource Availability: Unavailable</a>" ;
		}
		else{
			return "<a href='search.cgi?action=availability&keyword=$encoded_keyword&facet1=$facet1&facet2=$facet2&facet3=$facet3&meta=T&expand=$expand&lang=$lang&random=$random&db=$db' onclick=\"showLoadingImg()\">Resource Availability: Available</a>" ;
		}
		
	}
	else{
		if($lastModified eq "F"){
			return "<a href='search.cgi?action=availability&keyword=$encoded_keyword&facet1=$facet1&facet2=$facet2&facet3=$facet3&meta=F&expand=$expand&lang=$lang&random=$random&db=$db' onclick=\"showLoadingImg()\">分譲: 不可</a>" ;
		}
		else{
			return "<a href='search.cgi?action=availability&keyword=$encoded_keyword&facet1=$facet1&facet2=$facet2&facet3=$facet3&meta=T&expand=$expand&lang=$lang&random=$random&db=$db' onclick=\"showLoadingImg()\">分譲: 可</a>" ;
		}
	}
	
}

sub editSeeAlso{
	my($rdoc) = @_;
	my $name = $rdoc->attr('@BiologicalDatabaseEntry_seeAlso_BiologicalDatabase_name');
	my $url = $rdoc->attr('@BiologicalDatabaseEntry_seeAlso_BiologicalDatabaseEntry_url');
	my $entry = $rdoc->attr('@BiologicalDatabaseEntry_seeAlso_BiologicalDatabaseEntry_entryID');
	my $link = "";
	
	
	if(defined($url)){
		my @urls = split(/,|;/,$url);
		my @names = ();
		my @entries = ();
		if(defined($name)){
			@names = split(/,|;/,$name);
		}
		if(defined($entry)){
			@entries = split(/,|;/,$entry);
		}
		$link .= "<br>";
		for(my $i=0;$i<$#urls+1;$i++){
			if($names[$i] =~ m/ICD\-10/i || $names[$i] =~ m/icd10/i){
				my $code = $entries[$i];
				$code  =~ s/\.//g;
				my $icd10 = ICD10->new;
				$icd10List = $icd10->getICD10($code);
			}
		#	$link .= "<a href=\"$urls[$i]\" target=\"new_\">" . $common->xss($names[$i]) . ": " . $common->xss($entries[$i]);
#DoBISCUIT対策 by Maori
        if(@entries == ()){
                $link .= "seeAlso: <a href=\"$urls[$i]\" target=\"new_\">" . $rdoc->attr('@BiologicalDatabaseEntry_seeAlso_BiologicalDatabaseEntry_name') . "</a>";
        }
        else{
                $link .= "seeAlso: - <a href=\"$urls[$i]\" target=\"new_\">" . $common->xss($names[$i]). " : " . $common->xss($entries[$i]). "</a>";
        }
			if($i!=$#urls){
				$link .= " - ";
			}
		}

	}
	return $link;
}

sub editCategory{
	my($rdoc) = @_;
	my $id = $rdoc->attr('#nodelabel');
	my $db = $rdoc->attr('@db');
	my $nodeurl = $rdoc->attr('#nodeurl');
	#my $db_property = $alldbs->{$id};
	
	if($nodeurl =~ m/jstage_/i){
		my $d = $alldbs->{jstage_all};
		$nodeurl = $d->[11];
	}
	
	if($db =~ m/jstage/i){
		my $d = $alldbs->{jstage_all};
		$nodeurl = $d->[11];
	}
	
	my $db_property = $db_nodeurl->{$nodeurl};
	
#	#例外処理
#	if($id eq "Ritsumeikan Univ."){
#		
#	}
#	else{
#		if(!defined($db_property)){
#			$db_property= $alldbs->{$db};
#			
#			if(!defined($db_property)){
#				my $ex = exception($id);
#				$db_property = $alldbs->{$ex};
#			}
#		}		
#	}

	#print $id . " " . $db;
	
	#例外処理
	

	
	my $length = @$db_property;
	my @facet1 = split(/;/,$db_property->[3]);
	my @facet2 = split(/;/,$db_property->[4]);
	my @facet3 = split(/;/,$db_property->[5]);
	my @facet1_name_en = split(/;/, $db_property->[$length-6]);
	my @facet2_name_en = split(/;/, $db_property->[$length-5]);
	my @facet3_name_en = split(/;/, $db_property->[$length-4]);
	my @facet1_ids = split(/;/, $db_property->[$length-3]);
	my @facet2_ids = split(/;/, $db_property->[$length-2]);
	my @facet3_ids = split(/;/, $db_property->[$length-1]);
	my $category = "";
	for(my $i=0;$i<$#facet1+1;$i++){
		my $name = $lang eq "" ? $facet1[$i] : $facet1_name_en[$i];
		$category .= "<a href='search.cgi?keyword=$encoded_keyword&action=facet1&facet1=$facet1_ids[$i]&facet2=$facet2&facet3=$facet3&expand=$expand&lang=$lang&random=$random' onclick=\"showLoadingImg()\">" . $common->xss( $name) . "</a>";
		if($i!=$#facet1){
			$category .= ", ";
		}
	}
	$category .= " | ";
	for(my $i=0;$i<$#facet2+1;$i++){
		my $name = $lang eq "" ? $facet2[$i] : $facet2_name_en[$i];
		$category .= "<a href='search.cgi?keyword=$encoded_keyword&action=facet1&facet1=$facet1&facet2=$facet2_ids[$i]&facet3=$facet3&expand=$expand&lang=$lang&random=$random' onclick=\"showLoadingImg()\">" . $common->xss($name) . "</a>";
		if($i!=$#facet2){
			$category .= ", ";
		}
	}
	$category .= " | ";
	for(my $i=0;$i<$#facet3+1;$i++){
		my $name = $lang eq "" ? $facet3[$i] : $facet3_name_en[$i];
		$category .= "<a href='search.cgi?keyword=$encoded_keyword&action=facet1&facet1=$facet1&facet2=$facet2&facet3=$facet3_ids[$i]&expand=$expand&lang=$lang&random=$random' onclick=\"showLoadingImg()\">" . $common->xss($name) . "</a>";
		if($i!=$#facet3){
			$category .= ", ";
		}
	}
	return $category;
}

sub editNodeLabel{
	my($rdoc) = @_;
	my $database = $rdoc->attr('@db');
	my $id = $rdoc->attr('#nodelabel');
	my $nodeurl = $rdoc->attr('#nodeurl');
#J-STAGE対策 by Maori 
	if($nodeurl =~ /jstage/){
	$nodeurl = "http://cs32.dbcls.jp:1978/node/jstage_all";
	}
	my $db_property = $db_nodeurl->{$nodeurl};
#	my $db_property = $alldbs->{$id};
#	if(!defined($db_property)){
#		$db_property= $alldbs->{$db};
#		
#		if(!defined($db_property)){
#			my $ex = exception($id);
#			$db_property = $alldbs->{$ex};
#		}
#	}
	my $encoded_meta = $common->xss_encode($database);
	my $dbLink = "<a href='search.cgi?action=db&keyword=$encoded_keyword&facet1=$facet1&facet2=$facet2&facet3=$facet3&meta=$encoded_meta&expand=$expand&lang=$lang&random=$random&db=$encoded_meta'  onclick=\"showLoadingImg()\" ";
	#if($lang eq ""){
		my $expla = $db_property->[12];
#		utf8::decode($expla); #utf8フラグをたてる
#		if(length($expla)>100){
#			$expla = substr($expla,0,150);
#		}
#		utf8::encode($expla);

		#英語版では説明を表示しない
		if($lang ne "en"){
			$dbLink .= " title='$expla' ";
		}
		
		
		my $db_id = $dbnames->{$dbs};
		#データベースが特許のとき
		if($database eq "jp_patent"){
			if(($action eq "db" && $meta eq $database) || ($db ne "" && $db eq $database)){
				$dbLink .= "style='color:#FF7010'>公表特許公報</a>";
			}
			elsif($dbs ne "" && defined($db_id)){
				$dbLink .= "style='color:#FF7010'>公表特許公報</a>";
			}
			else{
				$dbLink .= ">公表特許公報</a>";
			}	
		}
		#JStageのとき
		elsif($database =~ m/jstage_/i){
			if(($action eq "db" && $meta eq $database) || ($db ne "" && $db eq $database)  ){
				$dbLink .= "style='color:#FF7010'>J-STAGE</a>";
			}
			elsif($dbs ne "" && defined($db_id)){
				$dbLink .= "style='color:#FF7010'>J-STAGE</a>";
			}
			else{
				$dbLink .= ">J-STAGE</a>";
			}				
		}
		#特許以外のとき
		else{
		#ReaD&Researchmap対策 by Maori
			my $db_name = "";
			if($database eq "rrmap") {
                                $db_name = "ReaD&Researchmap";
                        }
                        else{
                                $db_name = $common->xss($db_property->[7]);
                        }
			if($action eq "db" && $meta eq $database){
				$dbLink .= "style='color:#FF7010'>" . $db_name . "</a>";
			}
			elsif($db ne "" && $db eq $database){
				$dbLink .= "style='color:#FF7010'>" . $db_name . "</a>";
			}
			elsif($dbs ne "" && defined($db_id) && $database eq $db_id){
				$dbLink .= "style='color:#FF7010'>" . $db_name . "</a>";
			}
			else{
				$dbLink .= ">" . $db_name . "</a>";
			}					
		}

		
	#}
	
	return $dbLink;
}

sub editSpecies{
	my($rdoc,$taxonomy) = @_;
	my $species = "";
	my $scientific_name = "";
	my $taxonid = "";
	my $display_name = "";
	if(defined($rdoc->attr('@species')) && defined($rdoc->attr('@taxonID'))){
		my $t = getTaxonomyFromID($taxonomy,$rdoc->attr('@taxonID'));
		my @taxon = @$t;
		if($#taxon!=-1){
			$scientific_name = $t->[0];
		}
		$display_name = $rdoc->attr('@species');
		$taxonid = $rdoc->attr('@taxonID');
	}
	elsif(defined($rdoc->attr('@species'))){
		$display_name = $rdoc->attr('@species');
	}
	elsif(defined($rdoc->attr('@taxonID'))){
		my $t = getTaxonomyFromID($taxonomy,$rdoc->attr('@taxonID'));
		my @taxon = @$t;
		if($#taxon!=-1){
			$scientific_name = $t->[0];
			$display_name = $t->[1];
		}
		$taxonid = $rdoc->attr('@taxonID');
		
	}
	elsif(defined($rdoc->attr('@BiologicalDatabaseEntry_taxon_BiologicalDatabaseEntry_name'))){
		$display_name = $rdoc->attr('@BiologicalDatabaseEntry_taxon_BiologicalDatabaseEntry_name');	
		$scientific_name = $display_name;
	}
	elsif(defined($rdoc->attr('@BiologicalDatabaseEntry_taxon_BiologicalDatabaseEntry_entryID'))){
		my $t = getTaxonomyFromID($taxonomy,$rdoc->attr('@BiologicalDatabaseEntry_taxon_BiologicalDatabaseEntry_entryID'));
		my @taxon = @$t;
		if($#taxon!=-1){
			$scientific_name = $t->[0];
			$display_name = $t->[1];
		}
		$taxonid = $rdoc->attr('@BiologicalDatabaseEntry_taxon_BiologicalDatabaseEntry_entryID');		
	}
	
	if($display_name ne ""){
		$species = editSpeciesTag($display_name,$taxonid,$scientific_name);
	}

	return $species;
}

sub getTaxonomyFromID{
	my($taxonomy,$taxonid) = @_;
	foreach my $element(@$taxonomy){
		if($element->[0] eq $taxonid){
			my @result = ($element->[1],$element->[2]);
			return \@result;
		}
	}
	return ();
}

sub editSpeciesTag{
	my ($display_name,$taxonid,$scientific_name) = @_;
		my $species = "";
		if($taxonid ne ""){
			my $encoded_meta = $common->xss_encode($taxonid);
			$species .= "<a href='search.cgi?action=taxonID&keyword=$encoded_keyword&facet1=$facet1&facet2=$facet2&facet3=$facet3&meta=$encoded_meta&expand=$expand&lang=$lang&random=$random&db=$db'  onclick=\"showLoadingImg()\" ";
		}
		else{
			my $encoded_meta = $common->xss_encode($display_name);
			$species .= "<a href='search.cgi?action=species&keyword=$encoded_keyword&facet1=$facet1&facet2=$facet2&facet3=$facet3&meta=$encoded_meta&expand=$expand&lang=$lang&random=$random&db=$db'  onclick=\"showLoadingImg()\" ";
			
		}
		my @names = split(/ /,$scientific_name);
		my $image_name = join("_",@names);
		my $image_path = "../..//sagace/images/icon/$image_name" . "_L.png";
		#print $image_path;
		my $config = SagaceConfig->new;
		$config->initialize();
		my $fullpath = $config->get(html_home) . "/sagace/images/icon/$image_name" . "_L.png";
		#print $fullpath;
		#print $scientific_name;
		if(-e $fullpath){
			my $name = $common->xss("<div align='center'>" . $scientific_name . "</div>");
			$species .= " title='$name' popimg='$image_path'";
		}
		#$species .= " >";
		#$species .= "<a href='search.cgi?action=$action&keyword=$encoded_keyword&meta=lang-ja&category=$facet' style='text-decoration: none;color:gray;'>";
		#Changed by Maori
		if(($action eq "taxonID" && $meta eq $taxonid) || $action eq "species" ){
			$species .= " style='color:#3C98C8;'> Species: " . $common->xss($display_name) . "";
		}#FF7010
		else{
			$species .= " style='color:##3C98C8;'>Species: " . $common->xss($display_name) . "";
		}#4a4a4a
		
		$species .= "</a>";
		return $species;
}

sub editID{
	my($rdoc) = @_;
	my $id = "";
	if(defined($rdoc->attr('@entryID'))){
		$id = "ID: " . $common->xss($rdoc->attr('@entryID'));
	}
	if(defined($rdoc->attr('@BiologicalDatabaseEntry_entryID'))){
		$id = "ID: " . $common->xss($rdoc->attr('@BiologicalDatabaseEntry_entryID'));
	}
	return $id;
}

sub editmolecule{
	my($rdoc) = @_;
	my $molecule = "";
	if(defined($rdoc->attr('@PDBo_struct.pdbx_descriptor'))){
		$molecule = "Molecule: " . $common->xss($rdoc->attr('@PDBo_struct.pdbx_descriptor'));
	}
	return $molecule;
}
sub editauthor{
	my($rdoc) = @_;
	my $author = "";
	if(defined($rdoc->attr('@PDBo_audit_author.name'))){
		$author = "Authors: " . $common->xss($rdoc->attr('@PDBo_audit_author.name'));
	}
	return $author;
}
#sub editSubdivide{
#	my($rdoc) = @_;
#	my $subdivide = "";
#	if(defined($rdoc->attr('@subdivide'))){
#		$subdivide = "<a href='search.cgi?action=subdivide&keyword=$encoded_keyword&facet1=$facet1&facet2=$facet2&meta=true&expand=$expand&lang=$lang' style='text-decoration: none;color:red;'> ";
#		$subdivide .= "分譲可";
#		$subdivide .= "</a>";
#	}
#	return $subdivide;	
#}


sub searchByID{
	my $self = shift;
	my($ids) = @_;
	#node_url、top_nodeのパラメータの取得。
	my $config = SagaceConfig->new;
	$config->initialize();
	my $node_master_url = $config->get("node_master_url");
	my $top_node = $config->get("top_node");
	
	my @db_hit_counts = ();
	
	# create and configure node
	#facet1,facet2 画面左のファセットタグをクリックしたとき
		
	# create condition
	my $cond = new Search::Estraier::Condition;
	#簡便書式
	#$cond->set_options('SIMPLE');
	#$cond->set_options('USUAL');
	$cond->set_options(qw/USUAL SIMPLE/ );
	
	#メタ情報での絞り込み
	if($action eq "species" || $action eq "db" || $action eq "taxonID"){
		$cond->add_attr("\@$action STREQ $meta");
	}
	
	
	foreach my $id(@$ids){
		$cond->add_attr("\@entryID ISTRINC $id");
	}
	
	# set search phrase
	#my $synonym_list = search_integ_dictionary($keyword);
	#同義語展開があるときとないとき
	if(!defined($expand) || $expand eq ""){
		my $search_phrase = createSearchPhrase();
		if($search_phrase ne ""){
			$cond->set_phrase($search_phrase);	
		}
				
	}
	else{
		my $search_phrase = createSearchPhrase_OffExpansion();
		if($search_phrase ne ""){
			$cond->set_phrase($search_phrase);
		}
	}
		
		
	my $node = new Search::Estraier::Node("$node_master_url/idsearch");
	#print "$node_master_url/idsearch";

		$node->set_snippet_width(150, 96, 	96); 
		#$node->set_timeout(5);#タイムアウトの時間設定 5秒	
		$cond->set_max(3);
		#検索を実行 
		#print $node->cond_to_query($cond,1);
		my $nres = $node->search($cond, 3);
		
		$self->{nres} = $nres;
}

1;
